# Alternatives to online services

## Some information to correlate some popular services
Social services are hosted on one single instance for e.g. there is only twitter.com or facebook.com.

Now consider Email service<br>
Even if people are using different email services they can still interact with each other if they know email address
for e.g. abc@my_mail.com can interact with xyz@ur_mail.com<br>
Similarly companies can host their own email services for internal interaction (work email).

If email analogy is extended to social services like twitter/facebook/instagram, then we can have multiple instances providing same 
service and person on instance1 can interact with person on instance2 same way as email. 

## List of services to be used as an alternative to tracking services
1. Search Engine
duckduckgo
https://duckduckgo.com/

2. Email
Protonmail / Tutanota
https://protonmail.com/
https://www.tutanota.com/

3. Social Networking:
mastodon<br> 
It is bit tricky to find users (who are not IRL friends)
Here are some links for discovery
https://joinmastodon.org/ (lists mastodon isntances)
https://instances.social/ (good way to find instance)
@FediFollows@mastodon.online (good recommendations to follow) currently most of the content is open source technologies

4. Photo Sharing:
pixelfed (pixelfed.org)<br>
Good instance: https://piconic.co

5. Broswer: Firefox<br>
If chrome / internet explorer engine is needed, use Chromium and Vivaldi (this one is useful filling DS160) respectively 

6. Navigation: Osmand<br>
AdressToGPS II is good app to search for address (online search) and send that to osmand for navigation

7. Messaging
Best: Instance of matrix/synapse (element.io)
2nd best: Signal (session is better option)
Lastly: Telegram (part of code is not open source)

8. Cloud storage / contacts / calendar:
Nextcloud with S3 bucket (for storage), there are few providers or run own server (https://nextcloud.com/)

9. Very good guide for Smartphone hardening (Android specific mostly)
https://dev.lemmy.ml/post/38770

